/**
 * 
 */
package edu.hawaii.ics211.src.edu.hawaii.ics211;

/**
 * TestCalculator tests Calculator class
 * @author Alexander Duong
 *
 */
public class TestCalculator {

  /**
   * @param args Takes string arguments
   */
  public static void main(String[] args) {
    Calculator x = new Calculator();
    System.out.println(x.add(4, 5));
    System.out.println(x.divide(3, 2));
    System.out.println(x.multiply(2, -2));
    System.out.println(x.subtract(1, -1));
    System.out.println(x.modulo(3, 2));
    System.out.println(x.pow(3, 3));

  }

}
